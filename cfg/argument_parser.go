package cfg

import (
	"bitbucket.org/krjura/kafka-tools-golang/errors"
	"flag"
	"strings"
)

type AppArguments struct {
	Config    string
	Topic     string
	Partition int
	Output    string
	From      string
	To        string
	Key       string
	Filter    []string
}

type ArgumentFilter struct {
	filters []string
}

func NewArgumentFilter() ArgumentFilter {
	return ArgumentFilter{filters: nil}
}

func (f *ArgumentFilter) String() string {
	if f.filters == nil {
		return ""
	} else {
		return strings.Join(f.filters, ",")
	}
}

func (f *ArgumentFilter) Set(value string) error {
	if f.filters == nil {
		f.filters = []string{value}
	} else {
		f.filters = append(f.filters, value)
	}

	return nil
}

func ParseArguments() (*AppArguments, error) {

	var config = flag.String("config", "config.json", "Location of the configuration file")
	var topic = flag.String("topic", "", "Name of the topic")
	var partition = flag.Int("partition", -1, "Id of the partition")
	var output = flag.String("output", "", "Filename where to store the result")
	var from = flag.String("from", "", "Timestamp of the first entry in ISO-8601 format e.g. 2007-12-03T10:15:30+01:00")
	var to = flag.String("to", "", "Timestamp of the last entry in ISO-8601 format e.g. 2007-12-03T10:15:30+01:00")
	var key = flag.String("key", "", "Filter messages based on key")
	var argumentFilter = NewArgumentFilter()
	flag.Var(&argumentFilter, "filter", "Filter messages based on JSON path expression. "+
		"Must be in the format <json path>;<operation>;<value>. "+
		"Supported operations are equals, contains, startsWith and endsWith")

	flag.Parse()

	if *topic == "" {
		return nil, errors.NewProcessingError("missing topic configuration parameter")
	}

	if *output == "" {
		return nil, errors.NewProcessingError("missing output configuration parameter")
	}

	return &AppArguments{
		Config:    *config,
		Topic:     *topic,
		Partition: *partition,
		Output:    *output,
		From:      *from,
		To:        *to,
		Key:       *key,
		Filter:    argumentFilter.filters},
		nil
}