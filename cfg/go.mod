module bitbucket.org/krjura/kafka-tools-golang/cfg

go 1.14

require (
	bitbucket.org/krjura/kafka-tools-golang/errors v1.0.0
)

replace bitbucket.org/krjura/kafka-tools-golang/errors => ../errors