package cfg

import (
	"bitbucket.org/krjura/kafka-tools-golang/errors"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
)

type JsonConfig struct {
	Config map[string]interface{}
}

func (c JsonConfig) Brokers() []string {
	val, ok := c.Config["kafka.props"]
	if !ok {
		fmt.Println("Configuration file is missing kafka.props. Using 127.0.0.1:9092 for broker list.")
		return []string{"127.0.0.1:9092"}
	}

	kafkaProps := val.(map[string]interface{})

	if kafkaProps == nil {
		fmt.Println("Configuration file is missing kafka.props. Using 127.0.0.1:9092 for broker list.")
		return []string{"127.0.0.1:9092"}
	}

	brokers, brokersOk := kafkaProps["bootstrap.servers"]

	if brokersOk == false || brokers == "" || brokers == nil {
		return []string{"127.0.0.1:9092"}
	} else {
		return []string{brokers.(string)}
	}
}

func LoadConfig(configFilename string) (*JsonConfig, error) {

	_, err := os.Stat(configFilename)
	if os.IsNotExist(err) {
		return nil, errors.NewProcessingError("file %s does not exists", configFilename)
	}

	data, err := ioutil.ReadFile(configFilename)
	if err != nil {
		return nil, errors.NewProcessingError("Unable to load configuration file: %s", err.Error())
	}

	jsonMap := make(map[string]interface{})
	err = json.Unmarshal(data, &jsonMap)
	if err != nil {
		return nil, errors.NewProcessingError("Unable to load configuration file: %s", err.Error());
	}

	return &JsonConfig{Config: jsonMap}, nil
}
