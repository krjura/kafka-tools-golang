package cfg_tests

import (
	"bitbucket.org/krjura/kafka-tools-golang/cfg"
	"testing"
)

func TestNewArgumentFilter(t *testing.T) {
	filter := cfg.NewArgumentFilter()

	if filter.String() != "" {
		t.Errorf("expecting empty string but got %s", filter.String())
	}

	filter = cfg.NewArgumentFilter()
	_  = filter.Set("127.0.0.1:9092")

	if filter.String() != "127.0.0.1:9092" {
		t.Errorf("expecting 127.0.0.1:9092 string but got %s", filter.String())
	}

	filter = cfg.NewArgumentFilter()
	_  = filter.Set("127.0.0.1:9092")
	_  = filter.Set("127.0.0.1:9093")

	if filter.String() != "127.0.0.1:9092,127.0.0.1:9093" {
		t.Errorf("expecting '127.0.0.1:9092,127.0.0.1:9093' but got '%s'", filter.String())
	}
}