module bitbucket.org/krjura/kafka-tools-golang/cfg_tests

go 1.14

require (
	bitbucket.org/krjura/kafka-tools-golang/cfg v1.0.0
)

replace bitbucket.org/krjura/kafka-tools-golang/cfg => ../cfg
replace bitbucket.org/krjura/kafka-tools-golang/errors => ../errors