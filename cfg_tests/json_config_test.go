package cfg_tests

import (
	"bitbucket.org/krjura/kafka-tools-golang/cfg"
	"fmt"
	"testing"
)

func TestJsonConfigLoadConfigWhenFileNotFound(t *testing.T) {
	_, err := cfg.LoadConfig("/tmp/1fc83950-ccca-4d48-8d69-a6383de11b35")

	if err != nil && err.Error() != "file /tmp/1fc83950-ccca-4d48-8d69-a6383de11b35 does not exists" {
		t.Errorf("expecting no error but got '%s'", err.Error())
	}
}

func TestJsonConfigLoadConfigWhenConfigEmpty(t *testing.T) {
	config, err := cfg.LoadConfig("../etc/testing/empty-config.json")

	if err != nil {
		t.Errorf("expecting no error but got '%s'", err.Error())
		return
	}

	if len(config.Config) != 0 {
		t.Errorf("expecting config map to be empty but got %s", config.Config)
	}
}

func TestJsonConfigLoadConfigWhenDefaultConfig(t *testing.T) {
	config, err := cfg.LoadConfig("../etc/docs/config-example.json")

	if err != nil {
		t.Errorf("expecting no error but got '%s'", err.Error())
		return
	}

	if len(config.Config) != 1 {
		t.Errorf("expecting config map to be empty but got %v", config.Config)
		return
	}

	kafkaProps, ok := config.Config["kafka.props"]
	if ok == false {
		t.Errorf("expecting config map to contain kafka.props but got %v", config.Config)
		return
	}

	kafkaPropsMap := kafkaProps.(map[string]interface{})

	expectations := [][]string{
		{ "bootstrap.servers", "127.0.0.1:9092"},
		{ "enable.auto.commit", "false"},
		{ "auto.offset.reset", "none"},
		{ "max.poll.records", "100"},
		{ "key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer"},
		{ "value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer"},
		{ "allow.auto.create.topics", "false" },
	}

	for _, expectation := range expectations {
		value, valueOk := kafkaPropsMap[expectation[0]]

		if valueOk == false {
			t.Errorf(
				"expecting value of key '%s' to be '%s' but key does not exists",
				expectations[0], expectations[1])
		}

		valueString := fmt.Sprintf("%v", value)
		if valueString != expectation[1] {
			t.Errorf(
				"expecting value of key '%s' to be '%s' but is %s",
				expectations[0], expectations[1], value)
		}
	}
}

func TestJsonConfigBrokersWhenBrokersNil(t *testing.T) {
	var configMap = map[string]interface{} {
		"kafka.props": map[string]interface{} {
			"bootstrap.servers": nil,
		},
	}

	config := cfg.JsonConfig{Config: configMap}

	if len(config.Brokers()) != 1 || config.Brokers()[0] != "127.0.0.1:9092" {
		t.Errorf("expecting brokers to be 127.0.0.1 but got %v", config.Brokers())
	}
}

func TestJsonConfigBrokersWhenBrokersEmptyString(t *testing.T) {
	var configMap = map[string]interface{} {
		"kafka.props": map[string]interface{} {
			"bootstrap.servers": "",
		},
	}

	config := cfg.JsonConfig{Config: configMap}

	if len(config.Brokers()) != 1 || config.Brokers()[0] != "127.0.0.1:9092" {
		t.Errorf("expecting brokers to be 127.0.0.1 but got %v", config.Brokers())
	}
}

func TestJsonConfigBrokersWhenBrokersExists(t *testing.T) {
	var configMap = map[string]interface{} {
		"kafka.props": map[string]interface{} {
			"bootstrap.servers": "127.0.0.1:9090",
		},
	}

	config := cfg.JsonConfig{Config: configMap}

	if len(config.Brokers()) != 1 || config.Brokers()[0] != "127.0.0.1:9090" {
		t.Errorf("expecting brokers to be 127.0.0.1:9090 but got %v", config.Brokers())
	}
}