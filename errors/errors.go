package errors

import "fmt"

type ProcessingError struct {
	message string
}

func (i ProcessingError) Error() string {
	return i.message
}

func NewProcessingError(message string, a ...interface{}) ProcessingError {
	return ProcessingError{message: fmt.Sprintf(message, a...)}
}
