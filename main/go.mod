module main

go 1.14

require (
	bitbucket.org/krjura/kafka-tools-golang/mfilters v1.0.0
	bitbucket.org/krjura/kafka-tools-golang/cfg v1.0.0
	bitbucket.org/krjura/kafka-tools-golang/errors v1.0.0
	github.com/PaesslerAG/gval v1.0.1 // indirect
	github.com/PaesslerAG/jsonpath v0.1.1 // indirect
	github.com/segmentio/kafka-go v0.3.5
)

replace bitbucket.org/krjura/kafka-tools-golang/mfilters => ../mfilters
replace bitbucket.org/krjura/kafka-tools-golang/cfg => ../cfg
replace bitbucket.org/krjura/kafka-tools-golang/errors => ../errors
