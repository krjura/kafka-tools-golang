package main

import (
	"bitbucket.org/krjura/kafka-tools-golang/cfg"
	"fmt"
	"os"
)

func main() {
	arguments, err := cfg.ParseArguments()

	if err != nil {
		fmt.Print(err.Error(), "\n")
		os.Exit(1)
	}

	config, err := cfg.LoadConfig(arguments.Config)

	if err != nil {
		fmt.Print(err.Error(), "\n")
		os.Exit(1)
	}

	printConfig(*arguments)

	// just partition
	if arguments.Partition == -1 {
		err  = readFromTopic(*arguments, *config)
	} else {
		err = readFromPartition(*arguments, *config)
	}

	if err != nil {
		fmt.Printf(err.Error())
	}
}

func printConfig(arguments cfg.AppArguments) {
	fmt.Printf("using arguments: \n\n"+
		"  config: %s\n"+
		"  output: %s\n"+
		"  topic: %s\n"+
		"  partition: %d\n"+
		"  from: %s\n"+
		"  to: %s\n"+
		"  key: %s\n"+
		"  filter: %v\n\n",
		arguments.Config,
		arguments.Output,
		arguments.Topic,
		arguments.Partition,
		arguments.From,
		arguments.To,
		arguments.Key,
		arguments.Filter)
}
