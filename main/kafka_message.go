package main

import (
	"encoding/json"
	"fmt"
	"github.com/segmentio/kafka-go"
	"os"
	"time"
)

type KafkaMessage struct {
	Topic     string
	Partition int
	Offset    int64
	Key       string
	Timestamp int64
	Value     interface{}
}

func toJson(km kafka.Message) []byte {
	msg, err := json.MarshalIndent(toKafkaMessage(km), "", "  ")

	if err != nil {
		fmt.Printf("cannot generate output json message: %s\n", err.Error())
		os.Exit(1)
	}

	return msg
}

func toKafkaMessage(km kafka.Message) KafkaMessage {
	return KafkaMessage{
		Topic:     km.Topic,
		Partition: km.Partition,
		Offset:    km.Offset,
		Key:       string(km.Key),
		Timestamp: km.Time.UnixNano() / int64(time.Millisecond),
		Value:     toValue(km)}
}

func toValue(km kafka.Message) interface{} {
	var value interface{} = nil

	valueMap := make(map[string]interface{})
	err := json.Unmarshal(km.Value, &valueMap)

	if err != nil {
		value = string(km.Value)
	} else {
		value = valueMap
	}

	return value
}
