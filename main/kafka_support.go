package main

import (
	"bitbucket.org/krjura/kafka-tools-golang/cfg"
	"bitbucket.org/krjura/kafka-tools-golang/mfilters"
	"container/list"
	"context"
	"fmt"
	"github.com/segmentio/kafka-go"
	"os"
	"time"
)

func readFromPartition(arguments cfg.AppArguments, config cfg.JsonConfig) error {
	writer, err := createWriter(arguments.Output)

	if err != nil {
		return err
	}

	defer writer.close()

	read(arguments, config, arguments.Partition, writer)

	return nil
}

func readFromTopic(arguments cfg.AppArguments, config cfg.JsonConfig) error {
	writer, err := createWriter(arguments.Output)

	if err != nil {
		return err
	}

	defer writer.close()

	for _, partitions := range getPartitions(arguments) {
		read(arguments, config, partitions.ID, writer)
	}

	return nil
}

func read(arguments cfg.AppArguments, config cfg.JsonConfig, partition int, writer *OutputWriter) {

	brokers := config.Brokers()
	if len(brokers) == 0 {
		fmt.Printf("No broker list defined. Using 127.0.0.1:9092 instead\n")
		brokers = []string{"127.0.0.1:9092"}
	}

	readerConfig := kafka.ReaderConfig{
		Brokers:   brokers,
		Topic:     arguments.Topic,
		Partition: partition}

	reader := kafka.NewReader(readerConfig)

	seek(arguments, reader)

	waitGo := make(chan string)
	kafkaReaderCtx, cancelFunc := context.WithCancel(context.Background())
	go timeoutKafkaReader(reader, cancelFunc, waitGo)

	filterList := buildFilters(arguments)

	for {
		km, err := reader.FetchMessage(kafkaReaderCtx)
		if err != nil && err.Error() != "context canceled" {
			fmt.Printf("kafka error: %s\n", err.Error())
			break
		} else if err != nil && err.Error() == "context canceled" {
			break
		} else if err != nil {
			fmt.Printf("kafka error: %s\n", err.Error())
			break
		}

		fmt.Printf(
			"read message from topic: %s, partition: %d and offset: %d\n",
			km.Topic, km.Partition, km.Partition)

		if keep(filterList, km) {
			err = writer.write(toJson(km))

			if err != nil {
				fmt.Printf(err.Error())
			}
		}

		waitGo <- "RESET"
	}
}

func seek(arguments cfg.AppArguments, reader *kafka.Reader) {

	var err error

	if arguments.From == "" {
		err = reader.SetOffset(0)
	} else {
		parsedFrom, err := time.Parse(time.RFC3339Nano, arguments.From)

		if err == nil {
			err = reader.SetOffsetAt(context.Background(), parsedFrom)
		}
	}

	if err != nil {
		fmt.Println("cannot set offset to kafka", err.Error())
		os.Exit(1)
	}
}

func keep(filterList *list.List, km kafka.Message) bool {
	for e := filterList.Front(); e != nil; e = e.Next() {
		var filter = e.Value.(mfilters.Filter)

		if filter.Keep(km) == false {
			return false
		}
	}

	return true
}

func buildFilters(arguments cfg.AppArguments) *list.List {
	filters := list.New()

	if arguments.Key != "" {
		filters.PushFront(mfilters.NewKeyFilter(arguments.Key))
	}

	if arguments.From != "" {

		filter, err := mfilters.NewFromFilter(arguments.From)
		if err != nil {
			fmt.Printf("Error creating from filter. Filter will be ignored. Error was: %s", err.Error())
		} else {
			filters.PushFront(filter)
		}
	}

	if arguments.To != "" {
		filter, err := mfilters.NewToFilter(arguments.To)
		if err != nil {
			fmt.Printf("Error creating to filter. Filter will be ignored. Error was: %s", err.Error())
		} else {
			filters.PushFront(filter)
		}
	}

	if arguments.Filter != nil {
		for _, argFilter := range arguments.Filter {

			filter, err := mfilters.NewJsonPathFilter(argFilter)
			if err != nil {
				fmt.Printf("Error creating JSON Path filter. Filter will be ignored. Error was: %s", err.Error())
			} else {
				filters.PushFront(filter)
			}
		}
	}

	return filters
}

func timeoutKafkaReader(reader *kafka.Reader, cancelFunc context.CancelFunc, reset chan string) {
	// fmt.Println("Kafka cancellation thread active")

	for {
		select {
		case <-reset:
			// do nothing
		case <-time.After(1 * time.Second):
			lag, err := reader.ReadLag(context.Background())
			if err == nil && lag == 0 {
				// fmt.Println("done receiving messages from kafka")
				cancelFunc()
			}
		}
	}
}

func getPartitions(arguments cfg.AppArguments) []kafka.Partition {
	conn, _ := kafka.DialContext(context.Background(), "tcp", "localhost:9092")
	defer conn.Close()

	partitions, err := conn.ReadPartitions(arguments.Topic)
	if err != nil {
		fmt.Printf("cannot read topic partitions: %s\n", err.Error())
		os.Exit(1)
	}

	return partitions
}
