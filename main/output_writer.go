package main

import (
	"bitbucket.org/krjura/kafka-tools-golang/errors"
	"bufio"
	"fmt"
	"os"
)

type OutputWriter struct {
	outputFilename string
	writer         *bufio.Writer
	file           *os.File
}

func (w OutputWriter) write(json []byte) error {
	_, err := w.writer.Write(json)

	if err != nil {
		fmt.Printf("cannot write to output file %s: %s\n", w.outputFilename, err.Error())
		return err
	}

	_, err = w.writer.WriteString("\n")

	if err != nil {
		fmt.Printf("cannot write to output file %s: %s\n", w.outputFilename, err.Error())
		return err
	}

	return nil
}

func (w OutputWriter) close() {
	err := w.writer.Flush()
	if err != nil {
		fmt.Printf("cannot flush output to filename %s: %s\n", w.outputFilename, err.Error())
		return
	}

	err = w.file.Close()
	if err != nil {
		fmt.Printf("cannot close output to filename %s: %s\n", w.outputFilename, err.Error())
		return
	}
}

func createWriter(outputFilename string) (*OutputWriter, error) {
	f, err := os.Create(outputFilename)

	if err != nil {
		return nil, errors.NewProcessingError("cannot write to file %s: %s\n", outputFilename, err.Error())
	}

	return &OutputWriter{
		outputFilename: outputFilename,
		writer:         bufio.NewWriter(f),
		file:           f},
		nil
}
