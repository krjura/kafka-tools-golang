package mfilters

import (
	"bitbucket.org/krjura/kafka-tools-golang/errors"
	"github.com/segmentio/kafka-go"
	"time"
)

type FromFilter struct {
	from time.Time
}

func NewFromFilter(from string) (*FromFilter, error) {
	parsedFrom, err := time.Parse(time.RFC3339Nano, from)

	if err != nil {
		return nil, errors.NewProcessingError("invalid from format for %s: %s", from, err.Error())
	}

	return &FromFilter{from: parsedFrom}, nil
}

func (f *FromFilter) Keep(message kafka.Message) bool {
	if f.from.Equal(message.Time) || f.from.Before(message.Time) {
		return true
	} else {
		return false
	}
}
