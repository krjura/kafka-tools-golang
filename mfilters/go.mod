module bitbucket.org/krjura/kafka-tools-golang/mfilters

go 1.14

require (
    bitbucket.org/krjura/kafka-tools-golang/errors v1.0.0
	github.com/PaesslerAG/gval v1.0.1
	github.com/PaesslerAG/jsonpath v0.1.1
	github.com/segmentio/kafka-go v0.3.5
)

replace bitbucket.org/krjura/kafka-tools-golang/errors => ../errors