package mfilters

import (
	"bitbucket.org/krjura/kafka-tools-golang/errors"
	"context"
	"encoding/json"
	"github.com/PaesslerAG/gval"
	"github.com/PaesslerAG/jsonpath"
	"github.com/segmentio/kafka-go"
	"strings"
)

type JsonPathFilter struct {
	query      string
	expression string
	operation  string
	value      string
	jsonEval   *gval.Evaluable
}

func NewJsonPathFilter(query string) (*JsonPathFilter, error) {
	parts := strings.Split(query, ";")

	if len(parts) != 3 {
		return nil,
			errors.NewProcessingError(
			"cannot parse JSON path expression. Expecting expression;operation;value but got %s\n",
			query)
	}

	builder := gval.Full(jsonpath.PlaceholderExtension())
	jsonEval, err := builder.NewEvaluable(parts[0])
	if err != nil {
		return nil, errors.NewProcessingError("invalid json path %s: %s\n", parts[0], err.Error())
	}

	if !(parts[1] == "equals" || parts[1] == "contains" || parts[1] == "startsWith" || parts[1] == "endsWith") {
		return nil, errors.NewProcessingError("invalid operation %s", parts[1])
	}

	return &JsonPathFilter{
		query:      query,
		expression: strings.TrimSpace(parts[0]),
		operation:  strings.TrimSpace(parts[1]),
		value:      strings.TrimSpace(parts[2]),
		jsonEval:   &jsonEval},
		nil
}

func (f *JsonPathFilter) Keep(message kafka.Message) bool {
	var jsonVal interface{}
	err := json.Unmarshal(message.Value, &jsonVal)

	if err != nil {
		return false
	}

	result, err := f.jsonEval.EvalString(context.Background(), jsonVal)

	if err != nil {
		return false
	}

	if result == f.query {
		return false
	}

	switch f.operation {
		case "equals":
			return result == f.value
		case "contains":
			return strings.Contains(result, f.value)
		case "startsWith":
			return strings.HasPrefix(result, f.value)
		case "endsWith":
			return strings.HasSuffix(result, f.value)
		default:
			return false
	}
}
