package mfilters

import "github.com/segmentio/kafka-go"

type KeyFilter struct {
	key string
}

func (f *KeyFilter) Keep(message kafka.Message) bool {
	if message.Key != nil && string(message.Key) == f.key {
		return true
	} else {
		return false
	}
}

func NewKeyFilter(key string) *KeyFilter {
	return &KeyFilter{key: key}
}
