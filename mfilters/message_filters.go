package mfilters

import (
	"github.com/segmentio/kafka-go"
)

type Filter interface {
	Keep(message kafka.Message) bool
}
