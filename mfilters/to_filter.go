package mfilters

import (
	"bitbucket.org/krjura/kafka-tools-golang/errors"
	"github.com/segmentio/kafka-go"
	"time"
)

type ToFilter struct {
	to time.Time
}

func NewToFilter(to string) (*ToFilter, error) {
	parsed, err := time.Parse(time.RFC3339Nano, to)

	if err != nil {
		return nil, errors.NewProcessingError("invalid from format for %s: %s", to, err.Error())
	}

	return &ToFilter{to: parsed}, nil
}

func (f *ToFilter) Keep(message kafka.Message) bool {
	if f.to.Equal(message.Time) || f.to.After(message.Time) {
		return true
	} else {
		return false
	}
}
