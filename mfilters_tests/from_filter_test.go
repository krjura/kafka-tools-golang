package mfilters_tests

import (
	"bitbucket.org/krjura/kafka-tools-golang/mfilters"
	"github.com/segmentio/kafka-go"
	"testing"
	"time"
)

func TestFromFilterWithNotIso8601(t *testing.T) {
	_, err := mfilters.NewFromFilter("not iso")

	if err == nil {
		t.Error("should have failed since from is not iso 8601 string")
	}
}

func TestFromFilterWithValidIso8601ToBeEqual(t *testing.T) {
	// 1583542800000
	filter, err := mfilters.NewFromFilter("2020-03-07T02:00:00.000+01:00")

	if err != nil {
		t.Error("filter creating was not successful")
		return
	}

	result := filter.Keep(defaultMessageWithTimestamp(1583542800))

	if result == false {
		t.Error("filter result should have been true")
	}
}

func TestFromFilterWithValidIso8601ToBeBefore(t *testing.T) {
	// 1583542800000
	filter, err := mfilters.NewFromFilter("2020-03-07T02:00:00.000+01:00")

	if err != nil {
		t.Error("filter creating was not successful")
		return
	}

	result := filter.Keep(defaultMessageWithTimestamp(1583542801))

	if result == false {
		t.Error("filter result should have been true")
	}
}

func TestFromFilterWithValidIso8601ToBeAfter(t *testing.T) {
	// 1583542800000
	filter, err := mfilters.NewFromFilter("2020-03-07T02:00:00.000+01:00")

	if err != nil {
		t.Error("filter creating was not successful")
		return
	}

	result := filter.Keep(defaultMessageWithTimestamp(1583542700))

	if result == true {
		t.Error("filter result should have been false")
	}
}

func defaultMessageWithTimestamp(timestamp int64) kafka.Message {
	return kafka.Message{
		Topic:     "test",
		Partition: 0,
		Offset:    0,
		Key:       []byte(""),
		Value:     []byte(""),
		Headers:   make([]kafka.Header, 0),
		Time:      time.Unix(timestamp, 0),
	}
}
