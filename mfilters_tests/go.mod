module bitbucket.org/krjura/kafka-tools/kafka-data-extractor-golang/mfilter_tests

go 1.14

require (
	bitbucket.org/krjura/kafka-tools-golang/mfilters v1.0.0
	github.com/segmentio/kafka-go v0.3.5
)

replace bitbucket.org/krjura/kafka-tools-golang/mfilters => ../mfilters
replace bitbucket.org/krjura/kafka-tools-golang/errors => ../errors
