package mfilters_tests

import (
	"bitbucket.org/krjura/kafka-tools-golang/mfilters"
	"github.com/segmentio/kafka-go"
	"testing"
	"time"
)

func TestJsonPathFilterWhenQueryIsInvalid(t *testing.T) {
	_, err := mfilters.NewJsonPathFilter("$.name")

	if err == nil {
		t.Error("should have failed due to invalid query")
	}

	_, err = mfilters.NewJsonPathFilter("$.nam;equals")

	if err == nil {
		t.Error("should have failed due to invalid query")
	}

	_, err = mfilters.NewJsonPathFilter("$.nam;noop;1")

	if err == nil {
		t.Error("should have failed due to invalid operation")
	}
}

func TestJsonPathFilterWhenEquals(t *testing.T) {
	filter, err := mfilters.NewJsonPathFilter("$.name;equals;tester")

	if err != nil {
		t.Error("should not have failed")
		return
	}

	result := filter.Keep(defaultKafkaMessageWithJsonPath())
	mustBeTrue(result, t)
}

func TestJsonPathFilterWhenNotEquals(t *testing.T) {
	filter, err := mfilters.NewJsonPathFilter("$.name;equals;tester2")

	if err != nil {
		t.Error("should not have failed")
		return
	}

	result := filter.Keep(defaultKafkaMessageWithJsonPath())
	mustBeFalse(result, t)
}

func TestJsonPathFilterWhenInvalidEquals(t *testing.T) {
	filter, err := mfilters.NewJsonPathFilter("$.name == 2;equals;tester2")

	if err != nil {
		t.Error("should not have failed")
		return
	}

	result := filter.Keep(defaultKafkaMessageWithJsonPath())
	mustBeFalse(result, t)
}

func TestJsonPathFilterWhenContains(t *testing.T) {
	filter, err := mfilters.NewJsonPathFilter("$.name;contains;test")

	if err != nil {
		t.Error("should not have failed")
		return
	}

	result := filter.Keep(defaultKafkaMessageWithJsonPath())
	mustBeTrue(result, t)
}

func TestJsonPathFilterWhenNotContains(t *testing.T) {
	filter, err := mfilters.NewJsonPathFilter("$.name;contains;abc")

	if err != nil {
		t.Error("should not have failed")
		return
	}

	result := filter.Keep(defaultKafkaMessageWithJsonPath())
	mustBeFalse(result, t)
}

func TestJsonPathFilterWhenInvalidContains(t *testing.T) {
	filter, err := mfilters.NewJsonPathFilter("$.name == 2;contains;abc")

	if err != nil {
		t.Error("should not have failed")
		return
	}

	result := filter.Keep(defaultKafkaMessageWithJsonPath())
	mustBeFalse(result, t)
}

func TestJsonPathFilterWhenStartsWith(t *testing.T) {
	filter, err := mfilters.NewJsonPathFilter("$.name;contains;test")

	if err != nil {
		t.Error("should not have failed")
		return
	}

	result := filter.Keep(defaultKafkaMessageWithJsonPath())
	mustBeTrue(result, t)
}

func TestJsonPathFilterWhenNotStartsWith(t *testing.T) {
	filter, err := mfilters.NewJsonPathFilter("$.name;contains;abc")

	if err != nil {
		t.Error("should not have failed")
		return
	}

	result := filter.Keep(defaultKafkaMessageWithJsonPath())
	mustBeFalse(result, t)
}

func TestJsonPathFilterWhenInvalidStartsWith(t *testing.T) {
	filter, err := mfilters.NewJsonPathFilter("$.name == 2;contains;abc")

	if err != nil {
		t.Error("should not have failed")
		return
	}

	result := filter.Keep(defaultKafkaMessageWithJsonPath())
	mustBeFalse(result, t)
}

func TestJsonPathFilterWhenEndsWith(t *testing.T) {
	filter, err := mfilters.NewJsonPathFilter("$.name;contains;er")

	if err != nil {
		t.Error("should not have failed")
		return
	}

	result := filter.Keep(defaultKafkaMessageWithJsonPath())
	mustBeTrue(result, t)
}

func TestJsonPathFilterWhenNotEndsWith(t *testing.T) {
	filter, err := mfilters.NewJsonPathFilter("$.name;contains;abc")

	if err != nil {
		t.Error("should not have failed")
		return
	}

	result := filter.Keep(defaultKafkaMessageWithJsonPath())
	mustBeFalse(result, t)
}

func TestJsonPathFilterWhenInvalidEndsWith(t *testing.T) {
	filter, err := mfilters.NewJsonPathFilter("$.name == 2;contains;abc")

	if err != nil {
		t.Error("should not have failed")
		return
	}

	result := filter.Keep(defaultKafkaMessageWithJsonPath())
	mustBeFalse(result, t)
}

func mustBeFalse(result bool, t *testing.T)  {
	if result == true {
		t.Error("result from filter should have been false")
	}
}

func mustBeTrue(result bool, t *testing.T)  {
	if result == false {
		t.Error("result from filter should have been true")
	}
}

func defaultKafkaMessageWithJsonPath() kafka.Message {
	return kafka.Message{
		Topic:     "TestJsonPathFilterWhenEquals",
		Partition: 0,
		Offset:    0,
		Key:       []byte(""),
		Value:     []byte(`{"name": "tester","surname": "uat"}`),
		Headers:   make([]kafka.Header, 0),
		Time:      time.Unix(1583542700, 0),
	}
}