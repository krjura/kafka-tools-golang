package mfilters_tests

import (
	"bitbucket.org/krjura/kafka-tools-golang/mfilters"
	"github.com/segmentio/kafka-go"
	"testing"
	"time"
)

func TestKeyFilterWithSameKey(t *testing.T) {
	// 1583542800000
	filter:= mfilters.NewKeyFilter("demo")

	result := filter.Keep(defaultKafkaMessageWithKey())

	if result == false {
		t.Error("filter result should have been true")
	}
}

func TestKeyFilterWithNotSame(t *testing.T) {
	// 1583542800000
	filter:= mfilters.NewKeyFilter("TestNotSameKeyFilter")

	result := filter.Keep(defaultKafkaMessageWithKey())

	if result == true {
		t.Error("filter result should have been false")
	}
}

func defaultKafkaMessageWithKey() kafka.Message {
	return kafka.Message{
		Topic:     "test",
		Partition: 0,
		Offset:    0,
		Key:       []byte("demo"),
		Value:     []byte(`{"name": "tester","surname": "uat"}`),
		Headers:   make([]kafka.Header, 0),
		Time:      time.Unix(1583542700, 0),
	}
}
