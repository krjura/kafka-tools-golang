package mfilters_tests

import (
	"bitbucket.org/krjura/kafka-tools-golang/mfilters"
	"github.com/segmentio/kafka-go"
	"testing"
	"time"
)

func TestToFilterWithNotIso8601(t *testing.T) {
	_, err := mfilters.NewToFilter("not iso")

	if err == nil {
		t.Error("should have failed since from is not iso 8601 string")
	}
}

func TestToFilterWithValidIso8601ToBeEqual(t *testing.T) {
	// 1583542800000
	filter, err := mfilters.NewToFilter("2020-03-07T02:00:00.000+01:00")

	if err != nil {
		t.Error("filter creating was not successful")
		return
	}

	result := filter.Keep(kafka.Message{
		Topic:     "TestToFilterWithValidIso8601ToBeEqual",
		Partition: 0,
		Offset:    0,
		Key:       []byte(""),
		Value:     []byte(""),
		Headers:   make([]kafka.Header, 0),
		Time:      time.Unix(1583542800, 0),
	})

	if result == false {
		t.Error("filter result should have been true")
	}
}

func TestToFilterWithValidIso8601ToBeBefore(t *testing.T) {
	// 1583542800000
	filter, err := mfilters.NewToFilter("2020-03-07T02:00:00.000+01:00")

	if err != nil {
		t.Error("filter creating was not successful")
		return
	}

	result := filter.Keep(kafka.Message{
		Topic:     "TestToFilterWithValidIso8601ToBeBefore",
		Partition: 0,
		Offset:    0,
		Key:       []byte(""),
		Value:     []byte(""),
		Headers:   make([]kafka.Header, 0),
		Time:      time.Unix(1583542801, 0),
	})

	if result == true {
		t.Error("filter result should have been false")
	}
}

func TestToFilterWithValidIso8601ToBeAfter(t *testing.T) {
	// 1583542800000
	filter, err := mfilters.NewToFilter("2020-03-07T02:00:00.000+01:00")

	if err != nil {
		t.Error("filter creating was not successful")
		return
	}

	result := filter.Keep(kafka.Message{
		Topic:     "TestToFilterWithValidIso8601ToBeAfter",
		Partition: 0,
		Offset:    0,
		Key:       []byte(""),
		Value:     []byte(""),
		Headers:   make([]kafka.Header, 0),
		Time:      time.Unix(1583542700, 0),
	})

	if result == false {
		t.Error("filter result should have been true")
	}
}
